import Head from 'next/head'
import Image from 'next/image'
import styles from '../styles/Home.module.css'
import Recipes from './recipes'

export default function Home() {
  return (
    <div>
      <Head>
        <title>Grocery Shopping Integration</title>
        <meta name="description" content="Grocery Shopping Integration Code Challenge" />
        <link rel="icon" href="/favicon.ico" />
      </Head>

      <main>
        <div className="max-w-screen-xl mx-auto p-5 sm:p-10 md:p-16">
          <div className="grid grid-cols-1 sm:grid-cols-2 md:grid-cols-3 gap-10">
            <Recipes />
          </div>
        </div>
      </main>

      <footer className={styles.footer}>
        <a
          target="_blank"
          rel="noopener noreferrer"
        >
          Created by Sergio Peralta
        </a>
      </footer>
    </div>
  )
}
