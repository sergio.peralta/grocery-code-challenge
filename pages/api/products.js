const baseUrl = "https://www.walmart.com/grocery/v4/api/products/search"
const storeId = 3748
const count = 1;

export default async (req, res) => {

    const term = req.query.term;
    const url = `${baseUrl}?storeId=${storeId}&query=${term}&count=1&page=1&offset=0`

    console.log(url);

    const options = {
        headers: { 
                  "accept": "*/*",
                  'Content-Type': 'application/json',
                  'User-Agent': 'RecipesApp'
                }
    };

    const data = await fetch(url, 
        options).then(response => response.json());

    res.status(200).json(data)
};
