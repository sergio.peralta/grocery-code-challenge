const recipes = require('../data/recipes.json');

export default (req, res) => {
  res.status(200).json(recipes);
}
