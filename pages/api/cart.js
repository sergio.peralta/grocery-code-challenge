const baseUrl = "https://www.walmart.com/grocery/v4/api/products/search"
const storeId = 3748
const count = 1;


const getUrl = (items = []) => {
    const base = "https://affil.walmart.com/cart/0/2";
    const url = `${base}?items=${items.join(",")}&veh=aff&sourceid=imp_Xk1U1eWRkxyJRw3wUx0Mo38zUkix7TWhry3qyg0&veh=aff&wmlspartner=imp_1789939&clickid=Xk1U1eWRkxyJRw3wUx0Mo38zUkix7TWhry3qyg0`
    return url
}

export default async (req, res) => {

    const items = req.query.items
    if(!items)
        res.status(404).send("Missing items parameters");

    const list = items.split(",")
    const url = getUrl(list)
    console.log("Calling wallmart create cart")
    console.log(url);

    const options = {
        headers: { 
                  "accept": "*/*",
                  'Content-Type': 'application/json',
                  'User-Agent': 'RecipesApp'
                }
    };

    const data = await fetch(url, 
        options).then(response => response.text());

    res.status(200).json(data)
};
