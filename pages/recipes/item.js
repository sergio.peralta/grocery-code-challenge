import { useContext, useEffect, useState, useReducer } from "react";
import { CartItemsContext } from "./recipe"
const productJson = require('../data/product.json')

const reducer = key => key + 1;

const Item = (item = {}) => {
    const info = item?.item
    const [product, setProduct] = useState({})
    const { setCartItem } = useContext(CartItemsContext)

    /*useEffect(() => {
        fetch(`/api/products?term=${info.name}`)
        .then(res => res.json())
        .then(output => { 
            if(output.products)
                setProduct(output.products[0])
            else {
                console.log('No response from walmart')
            }
        })
    }, [])*/

    useEffect(() => {
        console.log('fake api json response');
        setProduct(productJson)
        addItem(productJson)
    }, [])

    const [id, updateId] = useReducer(reducer, '0');
    function addItem(product = {}) {
        console.log("adding offerId "+ product.offerId)
        setCartItem({
            name: product.basic.name,
            id: product.offerId
        })
      updateId();
    }

    return ( 
        <a href="#" className="w-full border-t-2 border-gray-100 font-medium text-gray-600 py-4 px-4 w-full block hover:bg-gray-100 transition duration-150">
            <img src={info?.imageUrl} alt="" className="rounded-full h-6 shadow-md inline-block mr-2" />
                {info?.size} {info?.name}
            <span className="text-gray-400 text-sm mx-4">${product && product.store?.price?.displayPrice}</span>
        </a>
    );
}
 
export default Item;