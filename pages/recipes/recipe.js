import { useEffect, useState, createContext, useReducer } from "react";
import Item from "./item";

export const CartItemsContext = createContext({});

function reducer(state, item) {
    return [...state, item]
}

const Recipe = (data) => {
    const [cartItems, setCartItem] = useReducer(reducer, [])
    const { recipe } = data

    function handleCartClick() {
        console.log(`creating cart with ${cartItems.length} elements`);

        if(cartItems.length > 0) {
            console.log(cartItems)
            let uniqueItems = Array.from(new Set(cartItems.map(i => i.id)));
            fetch(`api/cart?items=${uniqueItems.join(",")}`).then(res => console.log(res))
        }
    }

    return ( 
        <CartItemsContext.Provider value={{ cartItems, setCartItem }}>
            <div className="rounded overflow-hidden shadow-lg flex flex-col" key={recipe?.id}>
                <a href="#"> <img className="w-full" src={recipe?.imageUrl} alt="Sunset in the mountains"/></a>
                <div className="px-6 py-4 mb-auto">
                    <div className="mb-3">
                        <a href="#" className="text-xs text-green-600 transition duration-500 ease-in-out">
                            [Preparation {recipe?.time}+ min] 
                        </a>
                        {recipe && recipe.tags?.map(name => 
                            <a key={name} href="#" className="text-xs text-indigo-600 transition duration-500 ease-in-out px-1">
                            {name}
                            </a>
                        )}
                    </div>
                    <a href="#" className="font-medium text-lg inline-block hover:text-indigo-600 transition duration-500 ease-in-out inline-block mb-2">{recipe?.name}</a>
                    <p className="text-gray-500 text-sm">
                        {recipe?.summary}
                    </p>
                </div>
                <div className="w-full">
                    {recipe && recipe.items?.map(item => 
                        <Item item={item} key={item.id} />
                    )}
                </div>
                <div className="bg-gray-200 flex flex-row-reverse px-4 py-3">
                    <button className="bg-blue-500 py-2 px-4 rounded text-white" onClick={handleCartClick}>Buy It</button>
                </div>
            </div>
        </CartItemsContext.Provider>
    );
}
 
export default Recipe;