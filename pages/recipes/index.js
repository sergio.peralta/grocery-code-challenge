import { useEffect, useState } from "react";
import Recipe from "./recipe";

const Recipes = () => {

    const [recipes, setRecipes] = useState([]);

    useEffect(() => {
        fetch('/api/recipes')
            .then(res => res.json())
            .then(recipes => setRecipes(recipes));
    }, [])

    return ( 
        <>
            {recipes && recipes.map(recipe => 
                <Recipe recipe={recipe} key={recipe.id} />
            )}
            {!recipes &&
                <div>No recipes yet</div>
            }
        </>
     );
}
 
export default Recipes;